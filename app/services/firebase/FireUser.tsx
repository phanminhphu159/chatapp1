import { initializeApp,getApp } from 'firebase/app';
import {getAuth,onAuthStateChanged,signInAnonymously} from "firebase/auth";
import { getDatabase, ref,serverTimestamp,push,onChildAdded,off, onValue,DatabaseReference } from "firebase/database";

class FireUser {
    constructor() {
        this.init()
        this.checkAuth()
    }
    

    init = () => {
        if (!getApp.length) {
            initializeApp({
                apiKey: "AIzaSyDXtlyGVJr4LAPqGZwZDu8lwy6UTVueLwY",
                authDomain: "chat-test2-4fcc4.firebaseapp.com",
                databaseURL: "https://chat-test2-4fcc4-default-rtdb.asia-southeast1.firebasedatabase.app",
                projectId: "chat-test2-4fcc4",
                storageBucket: "chat-test2-4fcc4.appspot.com",
                messagingSenderId: "205822215923",
                appId: "1:205822215923:web:bcf8f5fabbe89eae2bd986",
                measurementId: "G-R8J4R6VNFN"
            })
        }
    }


    checkAuth = () => {
        const auth = getAuth();
        onAuthStateChanged(auth, user => {
            if (user != null) {
                signInAnonymously(auth);
            }
        });
    }

    // send = messages => {
    //     messages.forEach(item => {

    //         const message = {
    //             text: item.text,
    //             timestamp: firebase.database.ServerValue.TIMESTAMP,
    //             // image: "https://picsum.photos/id/237/200/300",
    //             user: {
    //                 _id: item.user.name,
    //                 name: item.user.name,
    //                 avatar: item.user.avatar,
    //             }
    //         }

    //         this.db.push(message)
    //     });
    // }

    parse = message => {
        const {user, text, timestamp, image} = message.val()
        const {key: _id} = message
        const createdAt = new Date(timestamp)

        return {
            _id,
            createdAt,
            text,
            image,
            user
        }
    }
    


    // getData(userName){
    //     return new Promise((resolve,reject)=>{
    //         onChildAdded(this.db, (snapshot) => {
    //             console.log(snapshot);
    //             if (snapshot.val().name == userName){
    //                 resolve(snapshot.val().userID);    
    //             }
    //         }); 
  
    //   });
    // }

    // async getUserID(userName){
    //     console.log("Caller------------------------");
    //     const userID = await this.getData(userName);
    //     console.log("After waiting");
    //     return userID;
    // }


    getUserID = (userName) =>{
        return new Promise((resolve,reject)=>{
            console.log('fetching id');
            onChildAdded(this.db, (snapshot) => {
                if (snapshot.val().name == userName){
                    resolve(snapshot.val().userID);    
                }
            }); 
        });
    }

    getUser = (userID) =>{
        return new Promise((resolve,reject)=>{
            let temp = {};
            onChildAdded(this.db, async snapshot => {
                if (snapshot.val().userID == userID){
                    temp = await { name : snapshot.val().name, avatar : snapshot.val().avatar } 
                    resolve(temp);   
                }
            });
        });
    }

    off(){
        off(this.db);
    }

    
    get db(){
        const db = getDatabase();
        return ref(db,"user");
    }

    get uid(){
        const auth = getAuth();
        const user = auth.currentUser;
        return (user || {}).uid;
    }
}

export default FireUser