import { initializeApp, getApp } from 'firebase/app';
import {getAuth,onAuthStateChanged,signInAnonymously} from "firebase/auth";
import { getDatabase, ref,serverTimestamp,push,onChildAdded,off } from "firebase/database";
import FireRoom from "../../services/firebase/FireRoom"
class FireParticipants {
    constructor() {
        this.init()
        this.checkAuth()
    }
    
    init = () => {
        if (!getApp.length) {
            initializeApp({
                apiKey: "AIzaSyDXtlyGVJr4LAPqGZwZDu8lwy6UTVueLwY",
                authDomain: "chat-test2-4fcc4.firebaseapp.com",
                databaseURL: "https://chat-test2-4fcc4-default-rtdb.asia-southeast1.firebasedatabase.app",
                projectId: "chat-test2-4fcc4",
                storageBucket: "chat-test2-4fcc4.appspot.com",
                messagingSenderId: "205822215923",
                appId: "1:205822215923:web:bcf8f5fabbe89eae2bd986",
                measurementId: "G-R8J4R6VNFN"
            })
        }
    }


    checkAuth = () => {
        const auth = getAuth();
        onAuthStateChanged(auth, user => {
            if (user != null) {
                signInAnonymously(auth);
            }
        });
    }

    send = messages => {
        messages.forEach(item => {

            const message = {
                text: item.text,
                timestamp: serverTimestamp(),
                // image: "https://picsum.photos/id/237/200/300",
                user: {
                    _id: item.user.name,
                    name: item.user.name,
                    avatar: item.user.avatar,
                }
            }

            push(this.db,message)
        });
    }

    parse = message => {
        const {user, text, timestamp, image} = message.val()
        const {key: _id} = message
        const createdAt = new Date(timestamp)

        return {
            _id,
            createdAt,
            text,
            image,
            user
        }
    }
    
    getAllRoom = (userID)=>{
        return new Promise((resolve,reject)=>{
            let temp = [];
            let Fire_Room = new FireRoom();
            onChildAdded(this.db, async snapshot => {
                console.log(snapshot)
                if (snapshot.val().userID == userID){
                    // get room
                    let room = await Fire_Room.getRoom(snapshot.val().roomID);
                    temp.push(room); 
                    resolve(temp);
                }
            });
        });
    }

    getUserinRoom = roomID => {
        let user = [];
        onChildAdded(this.db, snapshot => {
            if (snapshot.val().roomID == roomID){
                user.push(snapshot.val().userID)
            }
        });
        return user;
    }

    off(){
        off(this.db);
    }

    get db(){
        const db = getDatabase();
        return ref(db,"participants");
    }

    get uid(){
        const auth = getAuth();
        const user = auth.currentUser;
        return (user || {}).uid;
    }
}

export default FireParticipants