import { initializeApp,getApp } from 'firebase/app';
import {getAuth,onAuthStateChanged,signInAnonymously} from "firebase/auth";
// import {push} from "firebase/database";
import {getStorage,ref,uploadBytes,getDownloadURL} from "firebase/storage";

class FireStorage {
    constructor() {
        this.init()
        this.checkAuth()
    }
    

    init = () => {
        if (!getApp.length) {
            initializeApp({
                apiKey: "AIzaSyDXtlyGVJr4LAPqGZwZDu8lwy6UTVueLwY",
                authDomain: "chat-test2-4fcc4.firebaseapp.com",
                databaseURL: "https://chat-test2-4fcc4-default-rtdb.asia-southeast1.firebasedatabase.app",
                projectId: "chat-test2-4fcc4",
                storageBucket: "chat-test2-4fcc4.appspot.com",
                messagingSenderId: "205822215923",
                appId: "1:205822215923:web:bcf8f5fabbe89eae2bd986",
                measurementId: "G-R8J4R6VNFN"
            })
        }
    }


    checkAuth = () => {
        const auth = getAuth();

        onAuthStateChanged(auth, user => {
            if (user != null) {
                signInAnonymously(auth);
            }
        });
    }

    handleUpload = async (uri) => {
        const storage = getStorage();
        let url = [];

        for (const element of uri) {
            const uuidv4 = require('uuid/v4');
            const uuid = uuidv4();
            let reference = ref(storage,`images/room1/` + uuid);
            
            let img = await fetch(element);
            let bytes = await img.blob();
            
            await uploadBytes(reference, bytes); // unhandled promise rejection: Maxinnum call stack size exceeded warning

            let temp = await getDownloadURL(reference); // unhandled promise rejection: Maxinnum call stack size exceeded warning
            url.push(temp);
        }
        return url;
    };

    get uid(){
        const auth = getAuth();
        const user = auth.currentUser;
        return (user || {}).uid;
    }
}

export default FireStorage