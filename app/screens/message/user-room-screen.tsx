import React, { FC,useState, useEffect } from "react"
import { View, ViewStyle, TextStyle, TouchableOpacity, Image, FlatList,ImageStyle} from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import {
  Text,
} from "../../components"
import { color, typography } from "../../theme"
import { MessageNavigatorParamList } from "../../navigators"
import {icons} from '../../components/icon/icons/index'
import FireUser from "../../services/firebase/FireUser"
import FireParticipants from "../../services/firebase/FireParticipants"
import {StackActions  } from "@react-navigation/native"


const FULL: ViewStyle = { flex: 1 }
const TEXT: TextStyle = {
  color: color.palette.white,
  fontFamily: typography.primary,
}
const BOLD: TextStyle = { fontWeight: "bold" }
const TITLE: TextStyle = {
  ...TEXT,
  ...BOLD,
  fontSize: 20,
  lineHeight: 38,
  textAlign: "center",
  color: 'black'
}
const VIEW_HEADER: ViewStyle = {
  flexDirection: 'row',
  marginTop: 0,
  paddingHorizontal: 10,
  alignItems: 'center',
  justifyContent: 'center',
}

const LIST_CONTAINER: ViewStyle = {
  alignItems: "center",
  flexDirection: "row",
  padding: 10,
  paddingBottom: 15
}
const IMAGE: ImageStyle = {
  borderRadius: 40,
  height: 70,
  width: 70,
}
const LIST_TEXT: TextStyle = {
  marginLeft: 10,
  color: 'black',
  fontWeight: 'bold'
}
const FLAT_LIST: ViewStyle = {
  paddingHorizontal: 10,
  paddingTop: 20
}



export const UserRoomScreen: FC<StackScreenProps<MessageNavigatorParamList, "user_room">> = observer(
  ({ navigation,route }) => {

    
    const [RoomChat, setRoomChat] = useState(null);
    const [user, setuser] = useState(null);
    const Name = route.params;
    const Fire_User = new FireUser();
    const Fire_Participants = new FireParticipants();

    useEffect(() => {
      async function fetchRoom() {
        // get userID
        console.log('goto fetch user id');
        let userID = await Fire_User.getUserID(Name);
        console.log('id');
        console.log(userID);
        if (userID != undefined || null || ""){ 
          // get all user's rooms
          console.log('ok');
          let Allroom = await Fire_Participants.getAllRoom(userID);
          console.log(Allroom);
          // set data for the flat list
          setRoomChat(Allroom);
          setuser(userID);
        }   
      }
      fetchRoom()
    }, []);

    
    const next = (roomID) => {
      navigation.dispatch(StackActions.replace('message_chat',{roomID: roomID, userID: user}));
    }
    const goBack = () =>  { 
      navigation.dispatch(StackActions.replace('message_home'));
    };

    return (
      <View testID="WelcomeScreen" style={FULL}>
      {/* // Header Design  */}
      <View style={VIEW_HEADER}>
        <TouchableOpacity onPress={goBack} style={{left: 15, position: 'absolute'}}>
          <Image style={{tintColor: "black", height: 25, width: 25}} resizeMode="cover" source={icons.left} />
        </TouchableOpacity>
        <View style={{}}>
          <Text style={TITLE}>My Conversations</Text>
        </View>
      </View>

      
      <FlatList
        contentContainerStyle={FLAT_LIST}
        data={RoomChat}
        keyExtractor={(item) => String(item.id)}
        renderItem={({ item }) => (
          <TouchableOpacity style={LIST_CONTAINER} onPress={() => next(item.id)} >
            <Image source={{ uri: item.image }} style={IMAGE} />
            <View>
              <Text style={LIST_TEXT}> {item.name}</Text>
              <Text style={{marginLeft: 15, marginVertical: 5, color:'black'}}>{item.text }</Text>
            </View>
          </TouchableOpacity>
        )}
      />
      </View>
    )
  },
)

